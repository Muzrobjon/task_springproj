package com.epam.rd.autotasks;

public interface Employee {
    String getName();

    String getPosition();
}

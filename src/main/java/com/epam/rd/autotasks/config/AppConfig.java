package com.epam.rd.autotasks.config;

import com.epam.rd.autotasks.Employee;
import com.epam.rd.autotasks.EmployeeImpl;
import com.epam.rd.autotasks.Task;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan(basePackages = "com.epam.rd.autotasks")
//@PropertySource("classpath:application.properties")
public class AppConfig {

    @Bean
    public Employee employee(Employee employeeImpl) {
        return new EmployeeImpl("Name", "positopn") {
        };
    }

    @Bean
    public Task task(@Value("New feature") String description,
                     @Qualifier("junior") Employee assignee,
                     @Qualifier("senior") Employee reviewer) {
        return new Task(description, assignee, reviewer);
    }

    @Bean
    @Lazy
    public Employee junior() {
        return new EmployeeImpl("John Doe","Junior Software Engineer");
    }
    @Bean
    @Lazy
    public Employee senior() {
        return new EmployeeImpl("Emily Brown","Senior Software Engineer");
    }
    // TODO: Add your implementation here.
}

package com.epam.rd.autotasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Task {
    //description, assignee and reviewer
    private String description;


    private Employee assignee;
    private Employee reviewer;


    public Task() {
    }


    @Autowired
    public Task(@Value("New feature") String description,
                @Qualifier("junior") Employee assignee,
                @Qualifier("senior") Employee reviewer) {
        this.description = description;
        this.assignee = assignee;
        this.reviewer = reviewer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Employee getAssignee() {
        return assignee;
    }

    public void setAssignee(EmployeeImpl assignee) {
        this.assignee = assignee;
    }

    public void setAssignee(Employee assignee) {
        this.assignee = assignee;
    }

    public void setReviewer(Employee reviewer) {
        this.reviewer = reviewer;
    }
    public Employee getReviewer() {
        return reviewer;
    }

    public void setReviewer(EmployeeImpl reviewer) {
        this.reviewer = reviewer;
    }

    // TODO: Add your implementation here.
}
